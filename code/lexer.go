package code


type ItemType int

const (
	Text = ItemType(iota)
	TagName
	TagParam
	TagClose
)

const (
	TagBeginChar = rune('[')
	TagEndChar = rune(']')
	TagCloseChar = rune('[')
)


type StateFn func(LexerState) StateFn

type Item struct {
	Type ItemType
	Text string
}

type LexerState struct {
	in			string
	pos, start	int
	out 		chan Item
}

func Lex(input string) <- chan Item {
	lexer := &LexerState{
		in:input,
		pos:-1,
		start:0,
		out:make(chan Item),
	}
	
	go lexing(lexer)
	
	return lexer.out;
}

function lexing(lexer *LexerState) {
	StateFn state = StText
	for state != nil {
		state = state(lexer)
	}
}

func StText(lexer LexerState) {
	//TODO implement
}






















