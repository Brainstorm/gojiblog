package main

import (
	"fmt"
	"html/template"
	"net/http"
	"os"
	"strconv"
	"time"

	"bitbucket.org/Brainstorm/gojiblog/page"
	//"bitbucket.org/Brainstorm/gojiblog/"
)

const (
	pageDir     = "page"
	dataDir     = "data"
	templateDir = dataDir + "/templates"
	staticDir   = dataDir + "static"
	cssDir      = staticDir + "/css"
)

const (
	mainTemplateFile = templateDir + "/main.html"
)

var mainTemplate *template.Template = template.Must(template.ParseFiles(mainTemplateFile))

func logReq(name string, req *http.Request) {
	fmt.Println("Http Request:\t", req.URL.Path)
	fmt.Println("Handler name:\t", name)
}

func home(w http.ResponseWriter, req *http.Request) {
	logReq("home", req)
	var err error

	qPath := req.URL.Path
	if qPath != "/" {
		http.Error(w, "Page could not be found!", 404)
		return
	}

	query := req.URL.Query()
	qPage := query["page"]
	uid := 0
	if len(qPage) > 0 {
		if uid, err = strconv.Atoi(qPage[0]); err != nil {
			uid = 0
		}
	}

	if page, e := page.Get(uid); e {
		//fmt.Fprintf(w, "<h1>%s</h1>\n", page.Title)
		//w.Write(page.Contents)
		mainTemplate.Execute(w, page)
	} else {
		http.Error(w, "Page could not be found!", 404)
		return
	}

}

func css(w http.ResponseWriter, req *http.Request) {
	logReq("css", req)
	//find what css template is requested
	path := req.URL.Path
	path = cssDir + path[len("/css"):]
	fmt.Println("Path", path)

	//TODO check for back refereces. Only allow access to files under the css-root.
	info, err := os.Stat(path)
	if err != nil {
		http.Error(w, "File Not Found!", http.StatusNotFound)
		return
	}

	if info.IsDir() {
		http.Error(w, "File Not Found!", http.StatusNotFound)
		return
	}

	http.ServeFile(w, req, path)
}

func registerHandlers() {
	http.HandleFunc("/", home)
	staticUrl := "/static/"
	staticHandler := http.FileServer(http.Dir(staticDir))
	staticHandler = http.StripPrefix(staticUrl, staticHandler)
	http.Handle(staticUrl, staticHandler)
}

func startServer() {
	srv := &http.Server{
		Addr:           ":8080",
		ReadTimeout:    300 * time.Second,
		WriteTimeout:   300 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	srv.ListenAndServe()
}

func acceptCommands() {
	var input string
	for {
		fmt.Print("> ")
		fmt.Scanf("%s", &input)

		switch input {
		case "quit":
			fallthrough
		case "stop":
			fallthrough
		case "exit":
			return
		case "hello":
			fmt.Println("world!")
		case "p.r":
			fallthrough
		case "page.reload":
			fallthrough
		case "r":
			fallthrough
		case "reload":
			//note unprotected async access to pages
			fmt.Print("Reloading all pages...  ")
			page.ReloadAll(pageDir)
			fmt.Println("Done")
		case "t.r":
			fallthrough
		case "template.reload":
			fmt.Print("Reloading all templates... ")
			t, err := template.ParseFiles(mainTemplateFile)
			if err != nil {
				fmt.Println("Reload failed!")
				fmt.Println(err)
				continue
			}
			mainTemplate = t
			fmt.Println("Done!")
		default:
			fmt.Println("Unknown Command: " + input)
		}

	}
}

func main() {
	fmt.Println("Starting server...")
	page.LoadAll(pageDir)
	registerHandlers()
	go startServer()
	fmt.Println("Serving...")
	acceptCommands()
}
