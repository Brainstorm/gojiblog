package page

import (
	"html/template"
	"bytes"
)

type Context struct {
	Title string
	Site Website
	Content []template.HTML
}

type Content interface {
	Html() template.HTML
}

type TemplateContent struct {
	Template *template.Template
	Context interface{}
}

func NewTemplateContent(templateFiles ... string) (Content, error) {
	t, err := template.ParseFiles(templateFiles...)
	if err != nil {
		return nil, err
	}
	return TemplateContent{Template: t, Context: nil}, nil
}

func (tpc TemplateContent) Html() template.HTML {
	buf := &bytes.Buffer{}
	tpc.Template.Execute(buf, tpc.Context)
	return template.HTML(buf.String())
}





