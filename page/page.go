package page

import (
		"encoding/json"
		"os"
		"fmt"
		"time"
		"path/filepath"
)

var Pages map[int]*Page

type Page struct {
	UID int
	Title string
	Timestamp time.Time
	Contents string
}

func init() {
	Pages = make(map[int]*Page)
}

func Get(uid int) (*Page, bool) {
	p, e := Pages[uid]
	return p, e
}

func ReloadAll(dir string) {
	Pages = make(map[int]*Page)
	LoadAll(dir)
}

func Load(UID int) (p *Page, err error) {
	filename := fmt.Sprintf("data/%x.page", UID)
	return LoadFile(filename)
}

func LoadFile(filename string) (p *Page, err error) {
	page := &Page{}
	
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	
	dec := json.NewDecoder(file)
	dec.Decode(page)
	
	return page, nil
}

func LoadAll(dir string) {
	walker := func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}
		if ext := filepath.Ext(path); ext == ".page" {
			page, _ := LoadFile(path)
			
			Pages[page.UID] = page
		}
		return nil
	}
	filepath.Walk(dir, walker)
}

func (page *Page) Save() error {
	filename := fmt.Sprintf("data/%x.page", page.UID)
	
	file, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer file.Close()
	
	enc := json.NewEncoder(file)
	enc.Encode(page)
	return nil
}


