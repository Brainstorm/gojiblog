package page

type Website struct {
	Name string
	
}

var spWebsite chan *Website = make(chan *Website, 1)

func init() {
	//Load from file on init?
	spWebsite <- &Website{Name:"GojiBlog"}
}

//Must be thread safe
func GetWebsite() Website {
	//Get
	website := <-spWebsite	// Get the salt/pepper shaker
	copy := *website		// Copy, needs a function if using more pointers...
	spWebsite <- website	// Put it back
	return copy
}

//Must be thread safe
func SetWebsite(site Website)  {
	//Update
	website := <-spWebsite	// Get it
	*website = site			// Put new value
	spWebsite <- website	// then put it back
}



